package appilancers.com.infolist_t;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Created by Aamir on 22/03/15.
 * Main Activity Class that initializes the main fragment
 */
public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Programmatically create and embedd the fragment at run time
        if (savedInstanceState == null) {
            FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            ListViewFragment fragment = new ListViewFragment();
            transaction.replace(R.id.fragment_container, fragment);
            transaction.commit();
        }

    }
}
