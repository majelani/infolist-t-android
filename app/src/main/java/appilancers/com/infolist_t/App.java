package appilancers.com.infolist_t;

/**
 * Created by Aamir on 22/03/15.
 * Custom Application class that stores local cache of JSON data stream and initializes ImageLoader
 */

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

public class App extends Application {


    private static final String USER_INFO = "user_info";
    private static final String KEY_JSON = "json";
    public static final long ANIMATION_DURATION_STANDARD = 400L;

    protected static App sInstance;

    // Shared preferences reference to load / store local cache
    private SharedPreferences mPrefs;

    public static App getInstance() {
        return sInstance;
    }

    public App() {
        sInstance = this;
    }

    public void onCreate() {
        sInstance = this;
        super.onCreate();

        // Get handle of local shared store
        mPrefs = getSharedPreferences(USER_INFO, Context.MODE_PRIVATE);

        // Initializing ImageLoader here
        // Create default options which will be used for every
        //  displayImage(...) call if no options will be passed to this method
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .build();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .build();
        ImageLoader.getInstance().init(config);
    }

    // Saving JSON string in local cache
    public void saveJson(String json) {
        mPrefs.edit().putString(KEY_JSON, json).commit();
    }

    // Deleting local JSON storage
    public void clearJson() { mPrefs.edit().remove(KEY_JSON).commit();}

    // Loading from local cache
    public String getJson() {
        return mPrefs.getString(KEY_JSON, null);
    }
}
