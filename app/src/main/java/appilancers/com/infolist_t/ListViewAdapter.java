package appilancers.com.infolist_t;

import android.app.Application;
import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;


/**
 * Created by Aamir on 22/03/15.
 * ListViewAdapter is data adapter for the List View and provides all the standard methods for data adapter
 */

public class ListViewAdapter extends BaseAdapter {

    // Inflator to create the list view
    private LayoutInflater mInflater;

    // mList contains the downloaded JSON data with title and all rows
    private Content.ListInfo mList;

    public ListViewAdapter(Context context) {
        mInflater = LayoutInflater.from(context);
    }

    // setList is used to populate the mList container
    public void setList(Content.ListInfo list) {
        mList = list;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {

        return (mList != null) ? mList.getItems().size() : 0;
    }

    @Override
    public Content.ItemInfo getItem(int position) {
        return mList.getItems().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // If convertView is null, create one else reused the existing one
        if(convertView == null) {
            convertView = mInflater.inflate(R.layout.list_view_item, parent, false);
            ViewHolder holder = new ViewHolder();
            holder.title = (TextView) convertView.findViewById(R.id.title);
            holder.description = (TextView) convertView.findViewById(R.id.description);
            holder.icon = (ImageView) convertView.findViewById(R.id.image);
            holder.progress = (ProgressBar) convertView.findViewById(R.id.imageProgressBar);
            convertView.setTag(holder);
        }

        final ViewHolder holder = ((ViewHolder)convertView.getTag());

        //set image bitmap to null so that image from reused view is not shown while new image is being loaded
        holder.icon.setImageBitmap(null);

        // Remove image for now
        holder.icon.setVisibility(View.GONE);

        // show Progress indicator
        holder.progress.setVisibility(View.VISIBLE);

        // Get the relevant item from storage
        Content.ItemInfo item = getItem(position);
        if(item != null) {

            // Set Title Text
            holder.title.setText(item.getTitle());

            // Set description Text
            holder.description.setText(item.getDescription());

            int size = holder.icon.getDrawable().getIntrinsicHeight();

                // Load image, decode it to Bitmap and return Bitmap to callback
            ImageLoader.getInstance().displayImage(item.getImageHref(), holder.icon, new SimpleImageLoadingListener() {
                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    // Do whatever you want with Bitmap
                    holder.icon.setVisibility(View.VISIBLE);
                    holder.icon.getLayoutParams().width = 150;
                    holder.icon.getLayoutParams().height = 150;
                    holder.progress.setVisibility(View.GONE);
                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {
                    holder.icon.getLayoutParams().width = FrameLayout.LayoutParams.WRAP_CONTENT;
                    holder.icon.getLayoutParams().height = FrameLayout.LayoutParams.WRAP_CONTENT;
                    holder.icon.setMaxHeight(0);
                    holder.icon.setMaxWidth(0);
                    holder.progress.setVisibility(View.GONE);
                }
            });
        }
        return convertView;
    }
    //use view holder to avoid call ups on each getView() call
    private static class ViewHolder {
        TextView title;
        TextView description;
        ImageView icon;
        ProgressBar progress;
    }
}

