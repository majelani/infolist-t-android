package appilancers.com.infolist_t;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by Aamir on 22/03/15.
 * This class represents the main List view implemented as a fragment
 */

public class ListViewFragment extends Fragment  implements LoadJSONTask.ITaskLoadJson {

    // List View
    private ListView mList;

    // Adapter for List View
    private ListViewAdapter mListAdapter;

    // Progress Bar while data is being fetched
    private ProgressBar mProgressBar;

    // Text to display when there is not data to display
    private TextView mNoContent;

    /**
     * The {@link android.support.v4.widget.SwipeRefreshLayout} that detects swipe gestures and
     * triggers callbacks in the app.
     */
    private SwipeRefreshLayout mSwipeRefreshLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    // BEGIN_INCLUDE (inflate_view)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_view_layout, container, false);

        // Retrieve the SwipeRefreshLayout and ListView instances
        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swiperefresh);

        mListAdapter = new ListViewAdapter(getActivity());
        mList = (ListView)view.findViewById(android.R.id.list);
        mList.setAdapter(mListAdapter);

        mProgressBar = (ProgressBar) getActivity().findViewById(R.id.progress_bar);
        mNoContent = (TextView) getActivity().findViewById(R.id.no_content);

        return view;
    }
    // END_INCLUDE (inflate_view)

    // BEGIN_INCLUDE (setup_views)
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        loadContent();

        // BEGIN_INCLUDE (setup_refreshlistener)
        /**
         * Implement {@link SwipeRefreshLayout.OnRefreshListener}. When users do the "swipe to
         * refresh" gesture, SwipeRefreshLayout invokes
         * {@link SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}. In
         * {@link SwipeRefreshLayout.OnRefreshListener#onRefresh onRefresh()}, call a method that
         * refreshes the content. Call the same method in response to the Refresh action from the
         * action bar.
         */
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                initiateRefresh();
            }
        });
        // END_INCLUDE (setup_refreshlistener)
    }
    // END_INCLUDE (setup_views)


    ///////////////////////////////////////////
    // Content operations
    ///////////////////////////////////////////

    private void loadContent() {

        if(!NetworkUtil.isNetworkAvailable(getActivity())) {
            Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            fillListFromCache();
            if(mListAdapter.getCount() == 0) {
                animateAppear(mNoContent);
            } else {
                animateAppear(mList);
            }
            return;
        }

        // Show the list view with animation
        animateAppear(mList);

        mNoContent.setVisibility(View.GONE);
        mProgressBar.setVisibility(View.VISIBLE);

        try {
            URL url = new URL(getString(R.string.url));
            LoadJSONTask loadJsonTask = new LoadJSONTask();
            loadJsonTask.setListener(this);
            loadJsonTask.execute(url);
        }
        catch (MalformedURLException e) {
            e.printStackTrace();
        }
    }

    // JSONLoaded is called after data is successfully loaded
    @Override
    public void onJsonLoaded(Content.ListInfo list) {
        mListAdapter.setList(list);
        mProgressBar.setVisibility(View.GONE);
        getActivity().setTitle(list.getListTitle());

        animateAppear(mList);
        onRefreshComplete();
    }

    // fillListFromCache retrieves the data from Local storage in cache
    private void fillListFromCache() {
        String lastJson = App.getInstance().getJson();
        if(lastJson != null) {
            Content.ListInfo list = new Gson().fromJson(lastJson, Content.ListInfo.class);
            getActivity().setTitle(list.getListTitle());
            mListAdapter.setList(list);
        }
    }

    // If Refresh action does not find any new data, onNotNewData is called tha fetches data from local cache
    @Override
    public void onNoNewData() {
        mProgressBar.setVisibility(View.GONE);
        if(mListAdapter.getCount() == 0) {
            fillListFromCache();
        } else {
            Toast.makeText(getActivity(), getString(R.string.no_new_data), Toast.LENGTH_SHORT).show();
        }
        onRefreshComplete();
    }

    // onEmptyResult is called when no data is found on server
    @Override
    public void onEmptyResult() {
        mProgressBar.setVisibility(View.GONE);
        mList.setVisibility(View.INVISIBLE);
        mListAdapter.setList(null);
    }

    // onError is called when downloading is interrupted due to any error
    @Override
    public void onError() {
        mProgressBar.setVisibility(View.GONE);
        mList.setVisibility(View.INVISIBLE);
    }

    // BEGIN_INCLUDE (initiate_refresh)
    /**
     * By abstracting the refresh process to a single method, the app allows both the
     * SwipeGestureLayout onRefresh() method and the Refresh action item to refresh the content.
     */
    private void initiateRefresh() {

        /**
         * Execute the background task, which uses {@link android.os.AsyncTask} to load the data.
         */

        loadContent();
    }
    // END_INCLUDE (initiate_refresh)

    // BEGIN_INCLUDE (refresh_complete)
    /**
     * When the AsyncTask finishes, it calls onRefreshComplete(), which updates the data in the
     * ListAdapter and turns off the progress bar.
     */
    private void onRefreshComplete() {

        // Remove all items from the ListAdapter, and then replace them with the new items

        // Stop the refreshing indicator
        mSwipeRefreshLayout.setRefreshing(false);
    }
    // END_INCLUDE (refresh_complete)

    public void animateAppear(View v){
        v.setAlpha(0);
        v.setVisibility(View.VISIBLE);
        v.animate().alpha(1).setDuration(App.ANIMATION_DURATION_STANDARD);
    }

}
