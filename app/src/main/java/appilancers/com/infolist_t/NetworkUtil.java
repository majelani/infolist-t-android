package appilancers.com.infolist_t;

/**
 * Created by Aamir on 22/03/15.
 * NetworkUtil class that contains a function isNetworkAvailable to check if the network connectivity is available and return appropriate code
 */

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class NetworkUtil {

    /**
     * Checks if there is active Internet connection
     * @param context
     * @return <code>true</code> if there is an active Internet connection <code>false</code> otherwise
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        // Get the Network Info
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        // return the connection status
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
