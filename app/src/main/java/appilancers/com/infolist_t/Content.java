package appilancers.com.infolist_t;

/**
 * Created by Aamir on 22/03/15.
 * Content class maps the JSON data into corresponding class containers namely ItemInfo and ListInfo
 */

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.SerializedName;

//use serialized names for Gson to be able to convert json strings into objects
public class Content {

    // ItemInfo class represents the each data row in JSON with title, description and image URI
    public static class ItemInfo {
        @SerializedName("title")
        private String title;

        @SerializedName("description")
        private String description;

        @SerializedName("imageHref")
        private String imageHref;

        public ItemInfo(String title, String description, String image) {
            this.title = title;
            this.description = description;
            this.imageHref = image;
        }

        public String getTitle() {
            return title == null ? "" : title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description == null ? "" : description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getImageHref() {
            return imageHref == null ? "" : imageHref;
        }

        public void setImageHref(String imageHref) {
            this.imageHref = imageHref;
        }
    }

    // ListInfo represents higher level JSON container with title and row array as data
    public static class ListInfo {
        @SerializedName("title")
        private String listTitle;

        @SerializedName("rows")
        private List<ItemInfo> items;

        public String getListTitle() {
            return listTitle == null ? "" : listTitle;
        }

        public void setListTitle(String listTitle) {
            this.listTitle = listTitle;
        }

        public List<ItemInfo> getItems() {
            return items == null ? new ArrayList<ItemInfo>() : items;
        }

        public void setItems(List<ItemInfo> items) {
            this.items = items;
        }
    }
}
