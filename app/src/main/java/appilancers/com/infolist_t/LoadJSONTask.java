package appilancers.com.infolist_t;

/**
 * Created by Aamir on 22/03/15.
 * LoadJSONTask class provides the functionality to download the JSON data from Web Service
 * It extends AsyncTask and provides the required delegates for AsyncTask
 */

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.google.gson.stream.MalformedJsonException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class LoadJSONTask extends AsyncTask<URL, Void, Content.ListInfo> {

    // Codes for standard connection status
    private final int RESULT_NO_INTERNET = 0;
    private final int RESULT_OK = 1;
    private final int RESULT_NO_NEW_DATA = 2;
    private final int RESULT_ERROR = -1;
    private final int RESULT_EMPTY = 3;

    // Main Listener object. Listener object gets the feedback once the operations concludes either in success or failure
    private ITaskLoadJson mListener;
    private int mResult;

    public void setListener(ITaskLoadJson listener) {
        mListener = listener;
    }

    // doInBackground performs the actual download from JSON. With Local caching, UI is refreshed only if server has new data
    @Override
    protected Content.ListInfo doInBackground(URL... urls) {
        HttpURLConnection urlConnection = null;
        String jsonString;
        try {
            // Opening HTTPURLConnection
            urlConnection = (HttpURLConnection) urls[0].openConnection();

            // Opening Input Stream
            InputStream inSt = new BufferedInputStream(urlConnection.getInputStream());

            // Now Reading the Stream for data
            jsonString = readStream(inSt);

            // Fetching Local Cache version of data
            String lastJson = App.getInstance().getJson();

            // If server has same data as local cache, don't refresh the UI
            if(jsonString.equals(lastJson)) {
                mResult = RESULT_NO_NEW_DATA;
                return null;
            }

            // If server has new data, tell the UI (List View) to update with new data including image download
            if(jsonString != null && jsonString.length() != 0) {
                Content.ListInfo list = new Gson().fromJson(jsonString, Content.ListInfo.class);
                App.getInstance().saveJson(jsonString);
                mResult = RESULT_OK;
                return list;
            } else {
                mResult = RESULT_EMPTY;
            }
        } catch (MalformedJsonException e) {
            e.printStackTrace();
            mResult = RESULT_ERROR;
        } catch (IOException e) {
            e.printStackTrace();
            mResult = RESULT_ERROR;
        } finally {
            if(urlConnection != null)
                urlConnection.disconnect();
        }
        return null;
    }

    /**
     * Reads input stream and returns JSON substring
     * @param is input stream
     * @return JSON string
     */
    private String readStream(InputStream is) throws IOException{
        BufferedReader r = new BufferedReader(new InputStreamReader(is));
        StringBuilder builder = new StringBuilder();
        try {
            String line;
            while ((line = r.readLine()) != null) {
                builder.append(line);
            }
            int start = builder.indexOf("{");
            int end = builder.lastIndexOf("}");
            return builder.substring(start, end + 1);
        } finally {
            if(is != null)
                try {
                    is.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }
    }

    // onPostExecute is called after the background task is completed. It now can send message to its listener for appropriate actions
    @Override
    protected void onPostExecute(Content.ListInfo list) {
        if(mListener != null) {
            switch (mResult) {
                case RESULT_NO_NEW_DATA:
                    mListener.onNoNewData();
                    break;
                case RESULT_OK:
                    mListener.onJsonLoaded(list);
                    break;
                case RESULT_EMPTY:
                    mListener.onEmptyResult();
                    break;
                case RESULT_ERROR:
                    mListener.onError();
                    break;
            }
        }
    }

    /**
     * interface used to notify the caller of task status
     */
    public interface ITaskLoadJson {
        public void onJsonLoaded(Content.ListInfo list);
        public void onEmptyResult();
        public void onError();
        public void onNoNewData();
    }

}
